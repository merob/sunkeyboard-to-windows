# Sun Microsystems Keyboard To Windows

<div class="center">
<blockquote class="twitter-tweet"><p lang="en" dir="ltr">We had joy, we had fun<br>We ran Unix on a Sun,<br>But the source and the song<br>Of Solaris have all gone</p>&mdash; Michael Büker (@emtiu) <a href="https://twitter.com/emtiu/status/807117424957030400?ref_src=twsrc%5Etfw">December 9, 2016</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</div>

I found an old Sun Microsystems keyboard at about the same time Oracle binned Solaris. Had to use it. But control keys are in the wrong place. If for some reason you have to use Windows for work, this will re-map to standard ISO layout without the need to re-login (per user) while allowing you to switch back to standard laptop keyboard with a quick shortcut key.

![Sun Microsystems Type 6 Keyboard with labels on modified keys](/SunMicrosystemsType6.png) 

## Getting started

* Add the included AutoHotkey.ahk to Documents folder
* Download and install [AutoHotkey](https://www.autohotkey.com)
* Run shell:startup
* Add AutoHotKey to it
* Hit f9 to toggle - eg from built in keyboard to Sun Microsystems keyboard

## Limitations

* Tested with a [Type-6 keyboard](https://www.ebay.co.uk/sch/i.html?_nkw=Sun+microsystems+type+6). No idea if it works with others.
* Doesn't work until logged in. Eg, to login, ctrl+alt+delete will not be the right keys until you are logged in
* Doesn't work in remote desktop or probably Virtual machines if key strokes are passed through directly.
